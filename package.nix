{ stdenv, lib, gnumake, gcc }:

stdenv.mkDerivation (finalAttrs: {
    name = "kilo";
    src = ./.;
    nativeBuildInputs = [
        gnumake
    	gcc
    ];
    installPhase = ''
      install -D kilo $out/bin/kilo
    '';
})
